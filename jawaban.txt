Nomor 1: Membuat Database
Jawaban:
 create database myshop;

Nomor 2: Membuat Tabel Di Dalam Database
Jawaban:
* Membuat tabel user: 
MariaDB [myshop]> create table user(
    -> id int(8) auto_increment,
    -> name varchar(255),
    -> email varchar(255),
    -> password varchar(255),
    -> primary key(id)
    -> );
Query OK, 0 rows affected (0.042 sec)

* Membuat tabel category:
MariaDB [myshop]> create table category(
    -> id int(8) auto_increment,
    -> name varchar(255),
    -> primary key(id)
    -> );
Query OK, 0 rows affected (0.044 sec)

* Membuat tabel item:
MariaDB [myshop]> create table item(
    -> id int(8) auto_increment,
    -> name varchar(255),
    -> description varchar(255),
    -> price int(8),
    -> stock int(8),
    -> category_id int(8),
    -> primary key(id),
    -> foreign key(category_id) references category(id)
    -> );
Query OK, 0 rows affected (0.037 sec)

Nomor 3: Memasukkan Data pada Tabel
Jawaban:
* Memasukkan Data pada Tabel User:
MariaDB [myshop]> insert into user(name, email, password) values('John Doe','john@doe.com','john123'),('Jane Doe','jane@doe.com','jenita123');
Query OK, 2 rows affected (0.019 sec)
Records: 2  Duplicates: 0  Warnings: 0

* Memasukkan Data pada Tabel category:
MariaDB [myshop]>  insert into category(name) values("gadget"),("cloth"),("men"),("women"),("branded");
Query OK, 5 rows affected (0.003 sec)
Records: 5  Duplicates: 0  Warnings: 0

* Memasukkan Data pada Tabel Item:
insert into item(name, description, price, stock,  category_id) values('Sumsang b50','hape keren dari merek sumsang', 4000000, 100, 1),('Uniklooh','baju keren dari brand ternama', 500000, 50, 2),('IMHO Watch','jam tangan anak yang jujur banget', 2000000, 10, 1);
Query OK, 3 rows affected (0.015 sec)
Records: 3  Duplicates: 0  Warnings: 0

Nomor 4: Mengambil Data dari Database
Jawaban:
* Mengambil data users:
select id, name, email from user;

* Mengambil data items:
- sebuah query untuk mendapatkan data item pada table items yang memiliki harga di atas 1000000 (satu juta):
MariaDB [myshop]> select * form item where price > 1000000;

- sebuah query untuk mengambil data item pada table items yang memiliki name serupa atau mirip (like) dengan kata kunci “uniklo”, “watch”, atau “sang” (pilih salah satu saja):
select * from item where name like 'uniklo%';

* Menampilkan data items join dengan kategori:
 select item.id, item.name, item.description, item.price, item.stock, item.category_id, category.name from item inner join category on item.category_id;

 Nomor 5: Mengubah Data dari Database
 update item set price = 2500000 where name = "Sumsang b50";
